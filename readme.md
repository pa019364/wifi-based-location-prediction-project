
Deployment process:

Ubuntu (I'm on Ubuntu 18.04) or other Linux, or Unix like OSX
Other systems have not been adequately tested

1.
Install python3.6 or above

2. 
Install pip3

3.
(Optional, not required) (Create a PYTHon3 virtual directory to isolate the interaction between different versions of the library)
https://docs.python.org/zh-cn/3/tutorial/venv.html


4.
Run the requirements. TXT file in the requirements. TXT directory:
pip3 install --upgrade -r requirements.txt


5.
Run the results already trained to see, in terminal type:
python3 i2visual_and_analysis.py
Look at the visualizations and patterns

python3 i3rssi_to_predict.py
After running, press CTRL + Z to terminate and view simulation identification

